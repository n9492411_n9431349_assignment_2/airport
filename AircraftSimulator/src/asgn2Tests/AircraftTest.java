/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import asgn2Aircraft.A380;
import asgn2Aircraft.Aircraft;
import asgn2Aircraft.AircraftException;
import asgn2Aircraft.B747;
import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.first;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

/**
 * @author Marcus & Tony
 *
 */
public class AircraftTest {

    Aircraft A380plane;
    Aircraft B747plane;

    String A380flightCode = "A536";
    String B747flightCode = "B991";
    int departureTime = 50;
    int first = 10;
    int business = 20;
    int premium = 30;
    int economy = 40;

    Passenger Chuck_Norris;
    Passenger Chucky;
    Passenger Lee;
    Passenger Maia;

    /**
     * @throws AircraftException
     * 
     */
    @Before
    public void A380load() throws AircraftException {
        A380plane = new A380(this.A380flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);
    }

    /**
     * @throws AircraftException
     * 
     */
    @Before
    public void B747load() throws AircraftException {
        B747plane = new B747(this.B747flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);
    }

    /**
     * @throws PassengerException
     * @throws AircraftException
     */
    @Before
    public void Passengers() throws PassengerException, AircraftException {
        Chuck_Norris = new first(10, departureTime);
        Chucky = new Economy(10, departureTime);
        Lee = new Business(10, departureTime);
        A380plane.confirmBooking(Chucky, departureTime);
        A380plane.confirmBooking(Lee, departureTime);
    }

    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
     * .
     * 
     * @throws AircraftException
     * @throws PassengerException
     */
    @Test
    public void testAircraft() throws AircraftException, PassengerException {
        // fail("Not yet implemented");
        Aircraft A_plane = new A380(this.A380flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);
        Aircraft B_plane = new B747(this.B747flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);

        Chucky = new Economy(10, departureTime);
        Lee = new Business(10, departureTime);

        A_plane.confirmBooking(Chucky, departureTime);
        A_plane.confirmBooking(Lee, departureTime);
        assertEquals(A380plane.toString(), A_plane.toString());
        assertEquals(B747plane.toString(), B_plane.toString());
    }
    
    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#Aircraft(java.lang.String, int, int, int, int, int)}
     * .
     * 
     * @throws AircraftException
     * @throws PassengerException
     */
    @Test(expected = AircraftException.class)
    public void testAircraftFail() throws AircraftException, PassengerException {
        // fail("Not yet implemented");
        Aircraft A_plane = new A380(null, 0, -56, -96, -12,
                -1);
        Aircraft B_plane = new B747(null, this.departureTime, this.first, this.business, this.premium,
                this.economy);
    }

    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
     * .
     * 
     * @throws PassengerException
     * @throws AircraftException
     */
    @Test
    public void testCancelBooking() throws AircraftException, PassengerException {
        // fail("Not yet implemented");
        A380plane.cancelBooking(Chucky, 50);
        Aircraft A_plane = new A380(this.A380flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);
        Lee = new Business(10, departureTime);
        A_plane.confirmBooking(Lee, departureTime);
        assertEquals(A380plane.toString(), A_plane.toString());
    }
    
    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
     * .
     * 
     * @throws PassengerException
     * @throws AircraftException
     */
    @Test(expected = AircraftException.class)
    public void testCancelBookingAircraftException() throws AircraftException, PassengerException {
        // fail("Not yet implemented");
    	Passenger Jimbo = new first(10, departureTime);
        A380plane.cancelBooking(Jimbo, 50);
        Aircraft A_plane = new A380(this.A380flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);
        Lee = new Business(10, departureTime);
        A_plane.confirmBooking(Lee, departureTime);
        assertEquals(A380plane.toString(), A_plane.toString());
    }
    
    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}
     * .
     * 
     * @throws PassengerException
     * @throws AircraftException
     */
    @Test(expected = PassengerException.class)
    public void testCancelBookingPassengerException() throws AircraftException, PassengerException {
        // fail("Not yet implemented");
        A380plane.cancelBooking(Chucky, -50);
        Aircraft A_plane = new A380(this.A380flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);
        Lee = new Business(10, departureTime);
        A_plane.confirmBooking(Lee, departureTime);
        assertEquals(A380plane.toString(), A_plane.toString());
    }

    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}
     * .
     * 
     * @throws PassengerException
     * @throws AircraftException
     */
    @Test
    public void testConfirmBooking() throws AircraftException, PassengerException {
        // fail("Not yet implemented");
    	int numFirstBefore = A380plane.getNumFirst();
        A380plane.confirmBooking(Chuck_Norris, 50);
        int numFirstAfter = A380plane.getNumFirst();
        assertTrue(Chuck_Norris.isConfirmed());
        assertTrue(numFirstBefore == numFirstAfter);
    }
    
    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}
     * .
     * 
     * @throws PassengerException
     * @throws AircraftException
     */
    @Test (expected = AircraftException.class)
    public void testConfirmBookingAircraftException() throws AircraftException, PassengerException {
        // fail("Not yet implemented");
    	System.out.println("starts here gfdoifgfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfd");
    	Chuck_Norris.confirmSeat(1, 2);
    	for (int i = 0; i < 10; i++){
    		System.out.println("Check: " + i);
    		A380plane.confirmBooking(Chuck_Norris, 100);
    	}
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#finalState()}.
     * 
     * @throws AircraftException
     */
    @Test
    public void testFinalState() throws AircraftException {
        // fail("Not yet implemented");
        Aircraft B_plane = new B747(this.B747flightCode, this.departureTime, this.first, this.business, this.premium,
                this.economy);

        assertEquals(B747plane.finalState(), B_plane.finalState());
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#flightEmpty()}.
     */
    @Test
    public void testFlightEmpty() {
        // fail("Not yet implemented");
        assertFalse(A380plane.flightEmpty());
        assertTrue(B747plane.flightEmpty());

    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#flightFull()}.
     */
    @Test
    public void testFlightFull() {
        // fail("Not yet implemented");
        assertFalse(A380plane.flightFull());
        assertFalse(B747plane.flightFull());
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#flyPassengers(int)}.
     */
    @Test
    public void testFlyPassengers() {
        // fail("Not yet implemented");
    	
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getBookings()}.
     */
    @Test
    public void testGetBookings() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getNumBusiness()}.
     */
    @Test
    public void testGetNumBusiness() {
        // fail("Not yet implemented");
        // Not needed
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getNumEonomy()}.
     */
    @Test
    public void testGetNumEonomy() {
        // fail("Not yet implemented");
        // Not needed
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getNumFirst()}.
     */
    @Test
    public void testGetNumFirst() {
        // fail("Not yet implemented");
        // Not needed
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getNumPassengers()}.
     */
    @Test
    public void testGetNumPassengers() {
        // fail("Not yet implemented");
        // Not needed
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getNumPremium()}.
     */
    @Test
    public void testGetNumPremium() {
        // fail("Not yet implemented");
        // Not needed
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getPassengers()}.
     */
    @Test
    public void testGetPassengers() {
        // fail("Not yet implemented");
        System.out.println(A380plane.getPassengers());

    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#getStatus(int)}.
     */
    @Test
    public void testGetStatus() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#hasPassenger(asgn2Passengers.Passenger)}.
     */
    @Test
    public void testHasPassenger() {
        // fail("Not yet implemented");
        assertTrue(A380plane.hasPassenger(Lee));
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#initialState()}.
     */
    @Test
    public void testInitialState() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
     */
    @Test
    public void testSeatsAvailable() {
        // fail("Not yet implemented");
        assertTrue(B747plane.seatsAvailable(Maia));
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#toString()}.
     */
    @Test
    public void testToString() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
     */
    @Test
    public void testUpgradeBookings() {
        // fail("Not yet implemented");
    }

}
