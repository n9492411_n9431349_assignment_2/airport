/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author Tony Chau & Marcus Chan
 *
 */
public class EconomyTests {
	Passenger economy;
	int departureTime = 20;
	int BookingTime = 12;
	int cancellationTime = 21;
	@Before
	public void loadPassenger() throws PassengerException{
		economy = new Economy(this.BookingTime, this.departureTime);
	}
	/**
	 * Test method for {@link asgn2Passengers.Economy#noSeatsMsg()}.
	 */
	@Test
	public void testNoSeatsMsg() {
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#upgrade()}.
	 */
	@Test
	public void testUpgrade() {
		Passenger business = economy.upgrade();
		assertTrue(business instanceof Business);
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Economy#Economy(int, int)}.
	 */
	@Test
	public void testEconomy() {
//		fail("Not yet implemented");
	}

}
