package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.first;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

public class FirstTests {


	Passenger first;
	int departureTime = 20;
	int BookingTime = 12;
	int cancellationTime = 21;
	@Before
	public void loadPassenger() throws PassengerException{
		first = new first(this.BookingTime, this.departureTime);
	}
	@Test
	public void testNoSeatsMsg() {
		String message = "No seats available in First";
		assertTrue(message == first.noSeatsMsg());
//		fail("Not yet implemented");
	}

	@Test
	public void testUpgrade() {
		Passenger first2 = first.upgrade();
		assertTrue(first == first2);
//		fail("Not yet implemented");
	}

	@Test
	public void testFirstIntInt() throws PassengerException {
		Passenger first2 = new first(this.BookingTime, this.departureTime);
		assertFalse(first2 == first);
//		fail("Not yet implemented");
	}

	@Test
	public void testFirst() throws PassengerException {
        // fail("Not yet implemented");
        Passenger first_2 = new first(this.BookingTime, this.departureTime);
        assertTrue(first_2.isNew());
        assertFalse(first_2.isConfirmed());
        assertFalse(first_2.isQueued());
        assertFalse(first_2.isFlown());
        assertFalse(first_2.isRefused());

	}

	@Test
	public void testPassengerIntInt() {
//		fail("Not yet implemented");
	}

	@Test
	public void testPassenger() {
//		fail("Not yet implemented");
	}

	@Test
	public void testCancelSeat() throws PassengerException {
        // fail("Not yet implemented");
        first.confirmSeat(13, departureTime);

        first.cancelSeat(13);
        assertTrue(first.isNew());
        assertFalse(first.isConfirmed());
//		fail("Not yet implemented");
	}

	@Test
	public void testConfirmSeat() throws PassengerException {
//		fail("Not yet implemented");
        assertTrue(first.isNew());
        assertFalse(first.isConfirmed());
        assertFalse(first.isQueued());

        first.queuePassenger(13, departureTime);
        assertTrue(first.isQueued());
        assertFalse(first.isNew());

        first.confirmSeat(15, departureTime);
        assertTrue(first.isConfirmed());
        assertFalse(first.isQueued());
	}

	@Test
	public void testFlyPassenger() throws PassengerException {
//		fail("Not yet implemented");
        assertFalse(first.isFlown());
        assertFalse(first.isConfirmed());

        first.confirmSeat(13, departureTime);
        assertTrue(first.isConfirmed());

        first.flyPassenger(departureTime);
        assertTrue(first.isFlown());
        assertFalse(first.isConfirmed());
	}

	@Test
	public void testGetBookingTime() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetConfirmationTime() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetDepartureTime() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetEnterQueueTime() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetExitQueueTime() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetPassID() {
//		fail("Not yet implemented");
	}

	@Test
	public void testIsConfirmed() {
//		fail("Not yet implemented");
	}

	@Test
	public void testIsFlown() {
//		fail("Not yet implemented");
	}

	@Test
	public void testIsNew() {
//		fail("Not yet implemented");
	}

	@Test
	public void testIsQueued() {
//		fail("Not yet implemented");
	}

	@Test
	public void testIsRefused() {
//		fail("Not yet implemented");
	}

	@Test
	public void testQueuePassenger() throws PassengerException {
//		fail("Not yet implemented");
        assertTrue(first.isNew());
        assertFalse(first.isQueued());

        first.queuePassenger(13, departureTime);
        assertTrue(first.isQueued());
        assertFalse(first.isNew());
	}

	@Test
	public void testRefusePassenger() throws PassengerException {
//		fail("Not yet implemented");
        assertTrue(first.isNew());
        assertFalse(first.isRefused());
        assertFalse(first.isQueued());

        first.queuePassenger(13, departureTime);
        assertTrue(first.isQueued());
        assertFalse(first.isNew());

        first.refusePassenger(15);
        assertTrue(first.isRefused());
        assertFalse(first.isQueued());
	}

	@Test
	public void testToString() {
//		fail("Not yet implemented");
	}

	@Test
	public void testWasConfirmed() throws PassengerException {
        Passenger Private = new first(this.BookingTime, this.departureTime);
        assertFalse(Private.wasConfirmed());
        Private.confirmSeat(15, departureTime);
        assertTrue(Private.wasConfirmed());
//		fail("Not yet implemented");
	}

	@Test
	public void testWasQueued() throws PassengerException {
//		fail("Not yet implemented");
        Passenger Private = new first(this.BookingTime, this.departureTime);
        assertFalse(Private.wasQueued());
        Private.queuePassenger(13, departureTime);
        assertTrue(Private.wasQueued());
	}

	@Test
	public void testCopyPassengerState() {
//		fail("Not yet implemented");
	}

}
