/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.first;

/**
 * @author Tony Chau & Marcus Chan
 *
 */
public class BusinessTests {
	Passenger business;
	int departureTime = 20;
	int BookingTime = 12;
	int cancellationTime = 21;
	@Before
	public void loadPassenger() throws PassengerException{
		business = new Business(this.BookingTime, this.departureTime);
	}
	/**
	 * Test method for {@link asgn2Passengers.Business#noSeatsMsg()}.
	 */
	@Test
	public void testNoSeatsMsg() {
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#upgrade()}.
	 */
	@Test
	public void testUpgrade() {
		Passenger pass = business.upgrade();
		assertTrue(pass instanceof first);
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#Business(int, int)}.
	 * @throws PassengerException 
	 */
	@Test
	public void testBusinessIntInt() throws PassengerException {
		Passenger business2 = new Business(this.BookingTime, this.departureTime);
		assertFalse(business2 == business);
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Business#Business()}.
	 */
	@Test
	public void testBusiness() {
//		fail("Not yet implemented");
	}

}
