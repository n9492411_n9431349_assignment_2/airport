/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.first;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author Marcus
 *
 */
public class PassengerTest {

    Passenger first;
    Passenger business;
    Passenger premium;
    Passenger economy;

    int departureTime = 20;
    int BookingTime = 12;
    int cancellationTime = 21;

    /**
     * @throws PassengerException
     */
    @Before
    public void loadPassenger() throws PassengerException {
        first = new first(this.BookingTime, this.departureTime);
        business = new Business(this.BookingTime, this.departureTime);
        premium = new Premium(this.BookingTime, this.departureTime);
        economy = new Economy(this.BookingTime, this.departureTime);
        // test the changes in the time as well, but not sure how atm

    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#Passenger(int, int)}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testPassengerIntInt() throws PassengerException {
        // fail("Not yet implemented");
        Passenger first_2 = new first(this.BookingTime, this.departureTime);
        assertTrue(first_2.isNew());
        assertFalse(first_2.isConfirmed());
        assertFalse(first_2.isQueued());
        assertFalse(first_2.isFlown());
        assertFalse(first_2.isRefused());

        Passenger business_2 = new Business(this.BookingTime, this.departureTime);
        assertTrue(business_2.isNew());
        assertFalse(business_2.isConfirmed());
        assertFalse(business_2.isQueued());
        assertFalse(business_2.isFlown());
        assertFalse(business_2.isRefused());

        Passenger premium_2 = new Premium(this.BookingTime, this.departureTime);
        assertTrue(premium_2.isNew());
        assertFalse(premium_2.isConfirmed());
        assertFalse(premium_2.isQueued());
        assertFalse(premium_2.isFlown());
        assertFalse(premium_2.isRefused());

        Passenger economy_2 = new Economy(this.BookingTime, this.departureTime);
        assertTrue(economy_2.isNew());
        assertFalse(economy_2.isConfirmed());
        assertFalse(economy_2.isQueued());
        assertFalse(economy_2.isFlown());
        assertFalse(economy_2.isRefused());

    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#Passenger()}.
     */
    @Test
    public void testPassenger() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testCancelSeat() throws PassengerException {
        // fail("Not yet implemented");
        premium.confirmSeat(13, departureTime);
        assertTrue(premium.isConfirmed());
        assertFalse(premium.isNew());

        premium.cancelSeat(13);
        assertTrue(premium.isNew());
        assertFalse(premium.isConfirmed());
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testConfirmSeat() throws PassengerException {
        // fail("Not yet implemented");
        assertTrue(business.isNew());
        assertFalse(business.isConfirmed());
        assertFalse(business.isQueued());

        business.queuePassenger(13, departureTime);
        assertTrue(business.isQueued());
        assertFalse(business.isNew());

        business.confirmSeat(15, departureTime);
        assertTrue(business.isConfirmed());
        assertFalse(business.isQueued());
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testFlyPassenger() throws PassengerException {
        // fail("Not yet implemented");
        assertFalse(first.isFlown());
        assertFalse(first.isConfirmed());

        first.confirmSeat(13, departureTime);
        assertTrue(first.isConfirmed());

        first.flyPassenger(departureTime);
        assertTrue(first.isFlown());
        assertFalse(first.isConfirmed());
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#getBookingTime()}.
     */
    @Test
    public void testGetBookingTime() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#getConfirmationTime()}.
     */
    @Test
    public void testGetConfirmationTime() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#getDepartureTime()}.
     */
    @Test
    public void testGetDepartureTime() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#getEnterQueueTime()}.
     */
    @Test
    public void testGetEnterQueueTime() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#getExitQueueTime()}.
     */
    @Test
    public void testGetExitQueueTime() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#getPassID()}.
     */
    @Test
    public void testGetPassID() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#isConfirmed()}.
     */
    @Test
    public void testIsConfirmed() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#isFlown()}.
     */
    @Test
    public void testIsFlown() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#isNew()}.
     */
    @Test
    public void testIsNew() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#isQueued()}.
     */
    @Test
    public void testIsQueued() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#isRefused()}.
     */
    @Test
    public void testIsRefused() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#noSeatsMsg()}.
     */
    @Test
    public void testNoSeatsMsg() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testQueuePassenger() throws PassengerException {
        // fail("Not yet implemented");
        assertTrue(economy.isNew());
        assertFalse(economy.isQueued());

        economy.queuePassenger(13, departureTime);
        assertTrue(economy.isQueued());
        assertFalse(economy.isNew());
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testRefusePassenger() throws PassengerException {
        // fail("Not yet implemented");
        assertTrue(economy.isNew());
        assertFalse(economy.isRefused());
        assertFalse(economy.isQueued());

        business.queuePassenger(13, departureTime);
        assertTrue(business.isQueued());
        assertFalse(business.isNew());

        economy.refusePassenger(15);
        assertTrue(economy.isRefused());
        assertFalse(economy.isQueued());
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#toString()}.
     */
    @Test
    public void testToString() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#upgrade()}.
     */
    @Test
    public void testUpgrade() {
        // fail("Not yet implemented");
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#wasConfirmed()}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testWasConfirmed() throws PassengerException {
        // fail("Not yet implemented");
        Passenger Private = new first(this.BookingTime, this.departureTime);
        assertFalse(Private.wasConfirmed());
        Private.confirmSeat(15, departureTime);
        assertTrue(Private.wasConfirmed());
    }

    /**
     * Test method for {@link asgn2Passengers.Passenger#wasQueued()}.
     * 
     * @throws PassengerException
     */
    @Test
    public void testWasQueued() throws PassengerException {
        // fail("Not yet implemented");
        Passenger Private = new first(this.BookingTime, this.departureTime);
        assertFalse(Private.wasQueued());
        Private.queuePassenger(13, departureTime);
        assertTrue(Private.wasQueued());
    }

    /**
     * Test method for
     * {@link asgn2Passengers.Passenger#copyPassengerState(asgn2Passengers.Passenger)}
     * .
     */
    @Test
    public void testCopyPassengerState() {
        // fail("Not yet implemented");
    }

}
