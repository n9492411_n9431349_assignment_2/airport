/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author Tony
 *
 */
public class PremiumTests {
	Passenger premium;
	int departureTime = 20;
	int BookingTime = 12;
	int cancellationTime = 21;
	@Before
	public void loadPassenger() throws PassengerException{
		premium = new Premium(this.BookingTime, this.departureTime);
	}
	/**
	 * Test method for {@link asgn2Passengers.Premium#noSeatsMsg()}.
	 */
	@Test
	public void testNoSeatsMsg() {
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#upgrade()}.
	 */
	@Test
	public void testUpgrade() {
		Passenger pass = premium.upgrade();
		assertTrue(pass instanceof Business);
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#Premium(int, int)}.
	 * @throws PassengerException 
	 */
	@Test
	public void testPremiumIntInt() throws PassengerException {
		Passenger premium2 = new Premium(this.BookingTime, this.departureTime);
		assertFalse(premium2 == premium);
//		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link asgn2Passengers.Premium#Premium()}.
	 */
	@Test
	public void testPremium() {
//		fail("Not yet implemented");
	}

}
