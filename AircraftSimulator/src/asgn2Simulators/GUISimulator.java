/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Simulators;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import asgn2Aircraft.AircraftException;
import asgn2Passengers.PassengerException;

/**
 * @author hogan
 *
 */
@SuppressWarnings("serial")
public class GUISimulator extends JFrame implements Runnable, ActionListener {

    private int size = 1000;

    private int windows_Horizontal = (int) (size * 0.9);

    private int windows_Vertical = (int) (size * 0.5);

    private Dimension windows_Dimension = new Dimension(windows_Horizontal, windows_Vertical);

    private int verticalStruct = (int) (windows_Horizontal * 0.02);

    private int horizontalStruct = (int) (windows_Vertical * 0.05);

    private int space_Horizontal = (int) (size * 0.9);

    private int space_Vertical = (int) (size * 0.5);

    private Dimension space_Dimension = new Dimension(space_Horizontal, space_Vertical);

    private int graph_Horizontal = (int) (space_Horizontal * 0.9);

    private int graph_Vertical = (int) (space_Vertical * 0.9);

    private Dimension graph_Dimension = new Dimension(graph_Horizontal, graph_Vertical);

    private int textFieldSize = (int) (size * 0.02);

    private int button_Horizontal = (int) (size * 0.25);

    private int button_Vertical = (int) (button_Horizontal * 0.2);

    private Dimension button_Dimension = new Dimension(button_Horizontal, button_Vertical);

    private int titleText = (int) (size * 0.025);

    private ChartPanel passenger_chartPanel;

    private ChartPanel queue_refused_chartPanel;

    private Font textSize = new Font(Font.SANS_SERIF, 5, textFieldSize);

    private Font titleSize = new Font(Font.SANS_SERIF, Font.BOLD, titleText);

    private Font buttontSize = new Font(Font.SANS_SERIF, Font.BOLD, titleText);

    private static String GUItitle = ("Airport Simulator");

    private String simTitle = ("Simulation");

    private String classTitle = ("Fare Classes");

    private String operateTitle = ("Operation");

    private String graphChartTitle = ("Simulator Results");

    private JTextField seed;

    private JTextField mean;

    private JTextField queue;

    private JTextField cancel;

    private JTextField first;

    private JTextField business;

    private JTextField premium;

    private JTextField economy;

    private JButton simulationButton;

    private JButton showNextChartButton;

    private String simButtonText = ("Run Simulation");

    private String showButtonText = ("Show Next Chart");

    private ArrayList<Integer> firstList;

    private ArrayList<Integer> businessList;

    private ArrayList<Integer> premiumList;

    private ArrayList<Integer> economyList;

    private ArrayList<Integer> queueList;

    private ArrayList<Integer> refuseList;

    private ArrayList<Integer> totalList;

    private ArrayList<Integer> avaliableList;

    private XYSeriesCollection passengerdataset;

    private XYSeriesCollection queue_refuseddataset;

    private JPanel initialize;

    private Simulator sim;

    private Log log;

    private int page = 1;

    /**
     * arg0 sets the title immediately upon starting
     * 
     * the ChartPanel are set to empty instead of null or else null exception
     * will get called
     * 
     * @param arg0
     * @throws HeadlessException
     */
    public GUISimulator(String arg0) throws HeadlessException {
        super(arg0);
        // TODO Auto-generated constructor stub
        setLayout(new GridBagLayout());
        add(Box.createVerticalStrut(verticalStruct));
        setMinimumSize(windows_Dimension);

        String nullTitle = ("");
        final XYSeriesCollection dataset = new XYSeriesCollection();
        JFreeChart nullChart = ChartFactory.createXYLineChart(nullTitle, "", "", dataset, PlotOrientation.VERTICAL,
                true, true, false);
        passenger_chartPanel = new ChartPanel(nullChart);
        passenger_chartPanel.setVisible(false);

        queue_refused_chartPanel = new ChartPanel(nullChart);
        queue_refused_chartPanel.setVisible(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     * 
     * 
     * upon calling run is when the GUI truly starts
     * 
     * "initGui" is a method that start creating all the JPanels to fill in the
     * JFrame with "initialize" being the variable that holds all the JPanels so
     * it can be removed then re added as to update it
     * 
     * "showNextChartButton" being set to disabled being put here as to keep it
     * away from the rest of the code so as to never be called more than once
     */
    @Override
    public void run() {
        // TODO Auto-generated method stub
        // setContentPane(chartPanel);

        initialize = initGUI();
        add(initialize);
        showNextChartButton.setEnabled(false);
        pack();
        setVisible(true);

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        GUISimulator Gui = new GUISimulator(GUItitle);
        Gui.run();
    }

    /*
     * extra Methods
     */

    /**
     * as the name suggest the purpose of this method is to reset which serves
     * to update the GUI
     */
    public void reset() {
        remove(initialize);
        revalidate();
        repaint();
        initialize = initGUI();
        add(initialize);
        pack();
        setVisible(true);
    }

    /*
     * XYpassengerGraph creates the first graph which is called after running
     * simulation then immediately appears
     */
    private void XYpassengerGraph() {
        JFreeChart passengerXYChart = ChartFactory.createXYLineChart(graphChartTitle, "Days", "Passengers",
                graph_PassengerDataset(), PlotOrientation.VERTICAL, true, true, false);
        passenger_chartPanel = new ChartPanel(passengerXYChart);
        passenger_chartPanel.setMinimumSize(graph_Dimension);
        passenger_chartPanel.setPreferredSize(graph_Dimension);
        final XYPlot plot = (XYPlot) passengerXYChart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLACK);
        renderer.setSeriesPaint(1, Color.BLUE);
        renderer.setSeriesPaint(2, Color.CYAN);
        renderer.setSeriesPaint(3, Color.GRAY);
        renderer.setSeriesPaint(4, Color.GREEN);
        renderer.setSeriesPaint(5, Color.RED);
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));
        renderer.setSeriesStroke(1, new BasicStroke(2.0f));
        renderer.setSeriesStroke(2, new BasicStroke(2.0f));
        renderer.setSeriesStroke(3, new BasicStroke(2.0f));
        renderer.setSeriesStroke(4, new BasicStroke(2.0f));
        renderer.setSeriesStroke(5, new BasicStroke(2.0f));
        plot.setRenderer(renderer);
        passenger_chartPanel.revalidate();
        passenger_chartPanel.setVisible(true);
    }

    /*
     * graph_PassengerDataset finds then provides data for XYpassengerGraph
     */
    private XYSeriesCollection graph_PassengerDataset() {
        int days = 0;
        Integer previous = 0;
        Integer difference = 0;
        final XYSeries first = new XYSeries("First");
        for (int i = Constants.FIRST_FLIGHT; i < firstList.size(); i++) {
            days = i + 1;
            if (previous == 0) {
                first.add(days, firstList.get(i));
            } else if (previous > firstList.get(i)) {
                difference = (previous - firstList.get(i));
                first.add(days, difference);
            } else if (previous < firstList.get(i)) {
                difference = (firstList.get(i) - previous);
                first.add(days, difference);
            } else if (previous == firstList.get(i)) {
                difference = (previous - firstList.get(i));
                first.add(days, difference);
            }
            previous = firstList.get(i);
        }

        previous = 0;
        final XYSeries business = new XYSeries("Business");
        for (int i = Constants.FIRST_FLIGHT; i < businessList.size(); i++) {
            days = i + 1;
            if (previous == 0) {
                business.add(days, businessList.get(i));
            } else if (previous > businessList.get(i)) {
                difference = (previous - businessList.get(i));
                business.add(days, difference);
            } else if (previous < businessList.get(i)) {
                difference = (businessList.get(i) - previous);
                business.add(days, difference);
            } else if (previous == businessList.get(i)) {
                difference = (previous - businessList.get(i));
                business.add(days, difference);
            }
            previous = businessList.get(i);
        }

        previous = 0;
        final XYSeries premium = new XYSeries("Premium");
        for (int i = Constants.FIRST_FLIGHT; i < premiumList.size(); i++) {
            days = i + 1;
            if (previous == 0) {
                premium.add(days, premiumList.get(i));
            } else if (previous > premiumList.get(i)) {
                difference = (previous - premiumList.get(i));
                premium.add(days, difference);
            } else if (previous < premiumList.get(i)) {
                difference = (premiumList.get(i) - previous);
                premium.add(days, difference);
            } else if (previous == premiumList.get(i)) {
                difference = (previous - premiumList.get(i));
                premium.add(days, difference);
            }
            previous = premiumList.get(i);
        }

        previous = 0;
        final XYSeries economy = new XYSeries("Economy");
        for (int i = Constants.FIRST_FLIGHT; i < economyList.size(); i++) {
            days = i + 1;
            if (previous == 0) {
                economy.add(days, economyList.get(i));
            } else if (previous > economyList.get(i)) {
                difference = (previous - economyList.get(i));
                economy.add(days, difference);
            } else if (previous < economyList.get(i)) {
                difference = (economyList.get(i) - previous);
                economy.add(days, difference);
            } else if (previous == economyList.get(i)) {
                difference = (previous - economyList.get(i));
                economy.add(days, difference);
            }
            previous = economyList.get(i);
        }

        previous = 0;
        final XYSeries total = new XYSeries("Total");
        for (int i = Constants.FIRST_FLIGHT; i < totalList.size(); i++) {
            days = i + 1;
            if (previous == 0) {
                total.add(days, totalList.get(i));
            } else if (previous > totalList.get(i)) {
                difference = (previous - totalList.get(i));
                total.add(days, difference);
            } else if (previous < totalList.get(i)) {
                difference = (totalList.get(i) - previous);
                total.add(days, difference);
            } else if (previous == totalList.get(i)) {
                difference = (previous - totalList.get(i));
                total.add(days, difference);
            }
            previous = totalList.get(i);
        }

        previous = 0;
        final XYSeries avaliable = new XYSeries("Avaliable");
        for (int i = Constants.FIRST_FLIGHT; i < avaliableList.size(); i++) {
            days = i + 1;
            if (previous == 0) {
                avaliable.add(days, avaliableList.get(i));
            } else if (previous > avaliableList.get(i)) {
                difference = (previous - avaliableList.get(i));
                avaliable.add(days, difference);
            } else if (previous < avaliableList.get(i)) {
                difference = (avaliableList.get(i) - previous);
                avaliable.add(days, difference);
            } else if (previous == avaliableList.get(i)) {
                difference = (previous - avaliableList.get(i));
                avaliable.add(days, difference);
            }
            previous = avaliableList.get(i);
        }

        passengerdataset = new XYSeriesCollection();
        passengerdataset.addSeries(first);
        passengerdataset.addSeries(business);
        passengerdataset.addSeries(premium);
        passengerdataset.addSeries(economy);
        passengerdataset.addSeries(total);
        passengerdataset.addSeries(avaliable);
        return passengerdataset;
    }

    /*
     * XYqueue_refusedGraph creates the second graph which is called after
     * running simulation then appears after the showNextChartButton is pressed
     */
    private void XYqueue_refusedGraph() {
        JFreeChart passengerXYChart = ChartFactory.createXYLineChart(graphChartTitle, "Days", "Passengers",
                graph_Queue_RefusedDataset(), PlotOrientation.VERTICAL, true, true, false);
        queue_refused_chartPanel = new ChartPanel(passengerXYChart);
        queue_refused_chartPanel.setMinimumSize(graph_Dimension);
        queue_refused_chartPanel.setPreferredSize(graph_Dimension);
        final XYPlot plot = (XYPlot) passengerXYChart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLACK);
        renderer.setSeriesPaint(1, Color.RED);
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));
        renderer.setSeriesStroke(1, new BasicStroke(2.0f));
        plot.setRenderer(renderer);
        queue_refused_chartPanel.revalidate();
        queue_refused_chartPanel.setVisible(false);
    }

    /*
     * graph_Queue_RefusedDataset finds then provides data for
     * XYqueue_refusedGraph
     */
    private XYSeriesCollection graph_Queue_RefusedDataset() {
        int days = 0;
        final XYSeries queue = new XYSeries("Queue");
        for (int i = Constants.FIRST_FLIGHT; i < totalList.size(); i++) {
            days = i + 1;
            queue.add(days, queueList.get(i));
        }

        final XYSeries refuse = new XYSeries("Refused");
        for (int i = Constants.FIRST_FLIGHT; i < refuseList.size(); i++) {
            days = i + 1;
            refuse.add(days, refuseList.get(i));

        }

        queue_refuseddataset = new XYSeriesCollection();
        queue_refuseddataset.addSeries(queue);
        queue_refuseddataset.addSeries(refuse);
        return queue_refuseddataset;
    }

    /*
     * the initial state of the entire GUI
     * 
     * the spacePanel creates the space for the graph to exist with nextPanel
     * being the location for all the text fields and buttons
     */
    private JPanel initGUI() {
        JPanel GuiScreen = new JPanel();
        GuiScreen.setLayout(new BoxLayout(GuiScreen, BoxLayout.Y_AXIS));
        GuiScreen.add(Box.createVerticalStrut(verticalStruct));
        GuiScreen.add(spacePanel());
        GuiScreen.add(Box.createVerticalStrut(verticalStruct));
        GuiScreen.add(nextPanel());
        GuiScreen.add(Box.createVerticalStrut(verticalStruct));
        return GuiScreen;
    }

    /*
     * the space created for the Graph to exist
     */
    private JPanel spacePanel() {
        JPanel spacePanels = new JPanel();
        spacePanels.setLayout(new BoxLayout(spacePanels, BoxLayout.X_AXIS));
        spacePanels.add(Box.createHorizontalStrut(horizontalStruct));
        spacePanels.setMinimumSize(space_Dimension);
        spacePanels.setPreferredSize(space_Dimension);
        spacePanels.add(graphPanel());
        spacePanels.add(Box.createHorizontalStrut(horizontalStruct));
        return spacePanels;
    }

    /*
     * where the graph are being called to
     */
    private JPanel graphPanel() {
        JPanel graphPanels = new JPanel();
        graphPanels.setBackground(Color.WHITE);
        graphPanels.add(passenger_chartPanel, BorderLayout.CENTER);
        graphPanels.add(queue_refused_chartPanel, BorderLayout.CENTER);
        graphPanels.validate();
        return graphPanels;
    }

    /*
     * where all the text fields and buttons are being called to
     * 
     * simGroup is where the text fields for the simulation are being called
     * 
     * classGroup is where the text fields for the class are being called
     * 
     * operateGroup is where the buttons for the operation are being called
     * 
     */
    private JPanel nextPanel() {
        JPanel nextPanels = new JPanel();
        nextPanels.setLayout(new BoxLayout(nextPanels, BoxLayout.X_AXIS));
        nextPanels.add(Box.createHorizontalStrut(horizontalStruct));
        nextPanels.add(simGroup());
        nextPanels.add(Box.createHorizontalStrut(horizontalStruct));
        nextPanels.add(classGroup());
        nextPanels.add(Box.createHorizontalStrut(horizontalStruct));
        nextPanels.add(operateGroup());
        nextPanels.add(Box.createHorizontalStrut(horizontalStruct));
        return nextPanels;
    }

    /*
     * the title of the simulation is called first as to indicate the location
     * of the simulation text fields
     * 
     * then simulationPanel where the actual text fields are located are being
     * called
     */
    private JPanel simGroup() {
        JPanel simGroups = new JPanel();
        simGroups.setLayout(new BoxLayout(simGroups, BoxLayout.Y_AXIS));
        simGroups.add(panelTitle(simTitle));
        simGroups.add(Box.createVerticalStrut(verticalStruct));
        simGroups.add(simulationPanel());
        simGroups.add(Box.createVerticalStrut(verticalStruct));

        return simGroups;
    }

    /*
     * the title of the classes is called first as to indicate the location of
     * the classes text fields
     * 
     * then classesPanel where the actual text fields are located are being
     * called
     */
    private JPanel classGroup() {
        JPanel classGroups = new JPanel();
        classGroups.setLayout(new BoxLayout(classGroups, BoxLayout.Y_AXIS));
        classGroups.add(panelTitle(classTitle));
        classGroups.add(Box.createVerticalStrut(verticalStruct));
        classGroups.add(classesPanel());
        classGroups.add(Box.createVerticalStrut(verticalStruct));

        return classGroups;
    }

    /*
     * the title of the operations is called first as to indicate the location
     * of the operations text fields
     * 
     * then both the simPanleButton and the chartPanelButton where the actual
     * buttons are located are being called
     */
    private JPanel operateGroup() {
        JPanel operateGroups = new JPanel();
        operateGroups.setLayout(new BoxLayout(operateGroups, BoxLayout.Y_AXIS));
        operateGroups.add(panelTitle(operateTitle));
        operateGroups.add(Box.createVerticalStrut(verticalStruct));
        operateGroups.add(simPanelButton());
        operateGroups.add(Box.createVerticalStrut(verticalStruct));
        operateGroups.add(chartPanelButton());
        operateGroups.add(Box.createVerticalStrut(verticalStruct));

        return operateGroups;
    }

    /*
     * the simulationPanel groups the text fields with indicating labels
     */
    private JPanel simulationPanel() {
        JPanel simulationPanels = new JPanel();

        GroupLayout simulationLayout = new GroupLayout(simulationPanels);
        simulationPanels.setLayout(simulationLayout);

        simulationLayout.setAutoCreateGaps(true);
        simulationLayout.setAutoCreateContainerGaps(true);

        JLabel seedLabel = panelLabel("RNG Seed");
        JLabel meanLabel = panelLabel("Daily Mean");
        JLabel queueLabel = panelLabel("Queue Size");
        JLabel cancelLabel = panelLabel("Cancellation");

        seed = new JTextField(textFieldSize);
        mean = new JTextField(textFieldSize);
        queue = new JTextField(textFieldSize);
        cancel = new JTextField(textFieldSize);

        seed.setFont(textSize);
        mean.setFont(textSize);
        queue.setFont(textSize);
        cancel.setFont(textSize);

        seed.setText("" + Constants.DEFAULT_SEED);
        mean.setText("" + Constants.DEFAULT_DAILY_BOOKING_MEAN);
        queue.setText("" + Constants.DEFAULT_MAX_QUEUE_SIZE);
        cancel.setText("" + Constants.DEFAULT_CANCELLATION_PROB);

        GroupLayout.SequentialGroup horGroup = simulationLayout.createSequentialGroup();

        horGroup.addGroup(simulationLayout.createParallelGroup().addComponent(seedLabel).addComponent(meanLabel)
                .addComponent(queueLabel).addComponent(cancelLabel));
        horGroup.addGroup(simulationLayout.createParallelGroup().addComponent(seed).addComponent(mean)
                .addComponent(queue).addComponent(cancel));
        simulationLayout.setHorizontalGroup(horGroup);

        GroupLayout.SequentialGroup verGroup = simulationLayout.createSequentialGroup();

        verGroup.addGroup(
                simulationLayout.createParallelGroup(Alignment.CENTER).addComponent(seedLabel).addComponent(seed));
        verGroup.addGroup(
                simulationLayout.createParallelGroup(Alignment.CENTER).addComponent(meanLabel).addComponent(mean));
        verGroup.addGroup(
                simulationLayout.createParallelGroup(Alignment.CENTER).addComponent(queueLabel).addComponent(queue));
        verGroup.addGroup(
                simulationLayout.createParallelGroup(Alignment.CENTER).addComponent(cancelLabel).addComponent(cancel));
        simulationLayout.setVerticalGroup(verGroup);

        return simulationPanels;
    }

    /*
     * the classesPanel groups the text fields with indicating labels
     */
    private JPanel classesPanel() {
        JPanel classesPanels = new JPanel();

        GroupLayout classesLayout = new GroupLayout(classesPanels);
        classesPanels.setLayout(classesLayout);

        classesLayout.setAutoCreateGaps(true);
        classesLayout.setAutoCreateContainerGaps(true);

        JLabel firstLabel = panelLabel("First");
        JLabel businessLabel = panelLabel("Business");
        JLabel premiumLabel = panelLabel("Premium");
        JLabel economyLabel = panelLabel("Economy");

        first = new JTextField(textFieldSize);
        business = new JTextField(textFieldSize);
        premium = new JTextField(textFieldSize);
        economy = new JTextField(textFieldSize);

        first.setFont(textSize);
        business.setFont(textSize);
        premium.setFont(textSize);
        economy.setFont(textSize);

        first.setText("" + Constants.DEFAULT_FIRST_PROB);
        business.setText("" + Constants.DEFAULT_BUSINESS_PROB);
        premium.setText("" + Constants.DEFAULT_PREMIUM_PROB);
        economy.setText("" + Constants.DEFAULT_ECONOMY_PROB);

        GroupLayout.SequentialGroup horGroup = classesLayout.createSequentialGroup();

        horGroup.addGroup(classesLayout.createParallelGroup().addComponent(firstLabel).addComponent(businessLabel)
                .addComponent(premiumLabel).addComponent(economyLabel));
        horGroup.addGroup(classesLayout.createParallelGroup().addComponent(first).addComponent(business)
                .addComponent(premium).addComponent(economy));
        classesLayout.setHorizontalGroup(horGroup);

        GroupLayout.SequentialGroup verGroup = classesLayout.createSequentialGroup();

        verGroup.addGroup(
                classesLayout.createParallelGroup(Alignment.CENTER).addComponent(firstLabel).addComponent(first));
        verGroup.addGroup(
                classesLayout.createParallelGroup(Alignment.CENTER).addComponent(businessLabel).addComponent(business));
        verGroup.addGroup(
                classesLayout.createParallelGroup(Alignment.CENTER).addComponent(premiumLabel).addComponent(premium));
        verGroup.addGroup(
                classesLayout.createParallelGroup(Alignment.CENTER).addComponent(economyLabel).addComponent(economy));
        classesLayout.setVerticalGroup(verGroup);

        return classesPanels;
    }

    /*
     * the panelTitle method is what creates the titles above the two text
     * fields and the buttons
     */
    private JPanel panelTitle(String title) {
        JPanel pTitle = new JPanel();

        JLabel titleName = new JLabel();
        titleName.setText(title);
        titleName.setFont(titleSize);

        GroupLayout titleLayout = new GroupLayout(pTitle);
        pTitle.setLayout(titleLayout);

        titleLayout.setAutoCreateGaps(true);
        titleLayout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup horGroup = titleLayout.createSequentialGroup();

        horGroup.addGroup(titleLayout.createParallelGroup().addComponent(titleName));
        titleLayout.setHorizontalGroup(horGroup);

        GroupLayout.SequentialGroup verGroup = titleLayout.createSequentialGroup();

        verGroup.addGroup(titleLayout.createParallelGroup(Alignment.CENTER).addComponent(titleName));
        titleLayout.setVerticalGroup(verGroup);

        return pTitle;

    }

    /*
     * the simPanelButton calls the simulationButton and inputs it to this
     * location where upon pressing will run the simulator
     */
    private JPanel simPanelButton() {
        JPanel simButton = new JPanel();

        simulationButton = new JButton();
        simulationButton.setMinimumSize(button_Dimension);
        simulationButton.setPreferredSize(button_Dimension);
        simulationButton.setText(simButtonText);
        simulationButton.setFont(buttontSize);
        simulationButton.addActionListener(this);

        GroupLayout simLayout = new GroupLayout(simButton);
        simButton.setLayout(simLayout);

        simLayout.setAutoCreateGaps(true);
        simLayout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup horGroup = simLayout.createSequentialGroup();

        horGroup.addGroup(simLayout.createParallelGroup().addComponent(simulationButton));
        simLayout.setHorizontalGroup(horGroup);

        GroupLayout.SequentialGroup verGroup = simLayout.createSequentialGroup();

        verGroup.addGroup(simLayout.createParallelGroup(Alignment.CENTER).addComponent(simulationButton));
        simLayout.setVerticalGroup(verGroup);

        return simButton;
    }

    /*
     * the chartPanelButton calls the showNextChartButton and inputs it to this
     * location where upon pressing will switch between both chart after the
     * simulator has been run
     */
    private JPanel chartPanelButton() {
        JPanel chartButton = new JPanel();

        showNextChartButton = new JButton();
        showNextChartButton.setMinimumSize(new Dimension(button_Horizontal, button_Vertical));
        showNextChartButton.setPreferredSize(new Dimension(button_Horizontal, button_Vertical));
        showNextChartButton.setText(showButtonText);
        showNextChartButton.setFont(buttontSize);
        showNextChartButton.addActionListener(this);

        GroupLayout showLayout = new GroupLayout(chartButton);
        chartButton.setLayout(showLayout);

        showLayout.setAutoCreateGaps(true);
        showLayout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup horGroup = showLayout.createSequentialGroup();

        horGroup.addGroup(showLayout.createParallelGroup().addComponent(showNextChartButton));
        showLayout.setHorizontalGroup(horGroup);

        GroupLayout.SequentialGroup verGroup = showLayout.createSequentialGroup();

        verGroup.addGroup(showLayout.createParallelGroup(Alignment.CENTER).addComponent(showNextChartButton));
        showLayout.setVerticalGroup(verGroup);

        return chartButton;
    }

    /*
     * panelLabelis a method which is used for all labels so as to set all the
     * labels with the same settings
     */
    private JLabel panelLabel(String label) {

        JLabel text = new JLabel();
        text.setText(label);
        text.setFont(textSize);
        return text;

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     * 
     * whenever one of the buttons are pressed this method will activate and
     * deal go through with the action of said button accordingly
     * 
     * when showNextChartButton is pressed it uses the page int to switch
     * between the two graphs
     * 
     * when simulationButton is pressed it goes through takes the values within
     * the text field and imports them to the simulator class which then begins
     * running the simulator however if an incorrect value has been entered into
     * the text fiels then it will display a warning
     */
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        String seedText = seed.getText();
        String meanText = mean.getText();
        String queueText = queue.getText();
        String cancelText = cancel.getText();
        String firstText = first.getText();
        String businessText = business.getText();
        String premiumText = premium.getText();
        String economyText = economy.getText();
        Boolean seedBool = tryParse(seedText);
        Boolean meanBool = tryParse(meanText);
        Boolean queueBool = tryParse(queueText);
        Boolean cancelBool = tryParse(cancelText);
        Boolean firstBool = tryParse(firstText);
        Boolean businessBool = tryParse(businessText);
        Boolean premiumBool = tryParse(premiumText);
        Boolean economyBool = tryParse(economyText);
        if (src == simulationButton) {

            if (!seedBool || !meanBool || !queueBool || !cancelBool || !firstBool || !businessBool || !premiumBool
                    || !economyBool) {
                JOptionPane.showMessageDialog(this, "You have not fill in the simulation Box correctly", "Warning",
                        JOptionPane.WARNING_MESSAGE);
            } else {
                int seedInt = Integer.parseInt(seedText);
                Double meanDouble = Double.parseDouble(meanText);
                int queueInt = Integer.parseInt(queueText);
                Double cancelDouble = Double.parseDouble(cancelText);
                Double firstDouble = Double.parseDouble(firstText);
                Double businessDouble = Double.parseDouble(businessText);
                Double premiumDouble = Double.parseDouble(premiumText);
                Double economyDouble = Double.parseDouble(economyText);
                if (firstDouble + businessDouble + premiumDouble + economyDouble != 1 && cancelDouble <= 1) {
                    JOptionPane.showMessageDialog(this, "Classess inputs does not equal to 1", "Warning",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    Double standardDev = (meanDouble * 0.33);
                    Simulator sims;
                    Log log;
                    try {
                        sims = new Simulator(seedInt, queueInt, meanDouble, standardDev, firstDouble, businessDouble,
                                premiumDouble, economyDouble, cancelDouble);
                        try {
                            log = new Log();
                            try {
                                SimulationRunner(sims, log);
                                runSimulation();
                            } catch (AircraftException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            } catch (PassengerException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            } catch (IOException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            }
                        } catch (IOException e2) {
                            // TODO Auto-generated catch block
                            e2.printStackTrace();
                        }
                    } catch (SimulationException e2) {
                        // TODO Auto-generated catch block
                        e2.printStackTrace();
                    }

                }
            }
        } else if (src == showNextChartButton) {
            if (page == 1) {
                passenger_chartPanel.setVisible(false);
                queue_refused_chartPanel.setVisible(true);

            } else if (page == 2) {
                queue_refused_chartPanel.setVisible(false);
                passenger_chartPanel.setVisible(true);
                page = 0;
            }
            page = page + 1;
        }
    }

    /*
     * Checking if Input is valid or not
     */
    private Boolean tryParse(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            try {
                Double.parseDouble(text);
                return true;
            } catch (NumberFormatException i) {
                return false;
            }
        }
    }

    /**
     * before the simulation is ran this method SimulationRunner lays out the
     * foundations for the values to then be used to calculate the graph
     * 
     * @param sim
     * @param log
     */
    public void SimulationRunner(Simulator sim, Log log) {
        this.sim = sim;
        this.log = log;
        firstList = new ArrayList<Integer>();

        businessList = new ArrayList<Integer>();

        premiumList = new ArrayList<Integer>();

        economyList = new ArrayList<Integer>();

        queueList = new ArrayList<Integer>();

        refuseList = new ArrayList<Integer>();

        totalList = new ArrayList<Integer>();

        avaliableList = new ArrayList<Integer>();
    }

    /*
     * the main runner for generating the program
     */
    private void runSimulation() throws AircraftException, PassengerException, SimulationException, IOException {
        this.sim.createSchedule();
        this.log.initialEntry(this.sim);

        // Main simulation loop
        for (int time = 0; time <= Constants.DURATION; time++) {
            this.sim.resetStatus(time);
            this.sim.rebookCancelledPassengers(time);
            this.sim.generateAndHandleBookings(time);
            this.sim.processNewCancellations(time);
            if (time >= Constants.FIRST_FLIGHT) {
                this.sim.processUpgrades(time);
                this.sim.processQueue(time);
                this.sim.flyPassengers(time);
                this.sim.updateTotalCounts(time);
                this.log.logFlightEntries(time, sim);
            } else {
                this.sim.processQueue(time);
            }
            // Log progress
            this.log.logQREntries(time, sim);
            this.log.logEntry(time, this.sim);

            // Adding to the array list
            int fTotal = sim.getTotalFirst();
            int bTotal = sim.getTotalBusiness();
            int pTotal = sim.getTotalPremium();
            int eTotal = sim.getTotalEconomy();
            int nQTotal = sim.numInQueue();
            int nRTotal = sim.numRefused();
            int aTotal = sim.getTotalEmpty();
            firstList.add(fTotal);
            businessList.add(bTotal);
            premiumList.add(pTotal);
            economyList.add(eTotal);
            queueList.add(nQTotal);
            refuseList.add(nRTotal);
            totalList.add(fTotal + bTotal + pTotal + eTotal);
            avaliableList.add(aTotal);
        }
        this.sim.finaliseQueuedAndCancelledPassengers(Constants.DURATION);
        this.log.logQREntries(Constants.DURATION, sim);
        this.log.finalise(this.sim);
        showNextChartButton.setEnabled(true);
        XYpassengerGraph();
        XYqueue_refusedGraph();
        reset();
    }
}